import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_apache_is_installed(host):
    apache = host.package("apache2")

    assert apache.is_installed


def test_apache_is_running(host):
    apache = host.service("apache2")

    assert apache.is_running
    assert apache.is_enabled


def test_ufw_is_installed(host):
    ufw = host.package("ufw")

    assert ufw.is_installed


def test_ufw_is_configured(host):
    iptables = host.iptables.rules("filter", "ufw-user-input")
    http_rule_tcp = "-A ufw-user-input -p tcp -m tcp --dport 80 -j ACCEPT"
    http_rule_udp = "-A ufw-user-input -p udp -m udp --dport 80 -j ACCEPT"

    assert http_rule_tcp in iptables
    assert http_rule_udp in iptables


def test_ufw_is_running(host):
    ufw = host.service("ufw")

    assert ufw.is_running
    assert ufw.is_enabled
